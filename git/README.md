## Автоматизация администрирования инфраструктуры

8.1. [Git](https://github.com/guillotine666/nah/blob/master/git/notes/8-01.md) + [Практика](https://github.com/guillotine666/nah/blob/master/git/homeworks/8-01.md)

8.2. [Что такое DevOps. CI/CD.](https://github.com/guillotine666/nah/blob/master/git/notes/8-02.md) + [Практика](https://github.com/guillotine666/nah/blob/master/git/homeworks/8-02.md)

8.3. [GitLab. Практика](https://github.com/guillotine666/nah/blob/master/git/homeworks/8-03.md)
